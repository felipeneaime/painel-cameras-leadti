<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\Camera;
use Gate;
use App\Http\Controllers\App_Http_VideoStream;

class HomeController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth');
    }

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Camera $camera)
    {
        $cameras = $camera->all();
        //$cameras = $camera->where('user_id', auth()->user()->id)->get(); --Mostra somente itens de terminado usuario
        return view('home', compact('cameras'));
    }

    public function update($idCamera)
    {
        $camera = Camera::find($idCamera);


        //$this->authorize('update-camera', $camera);

         
        return view('camera-update', compact('camera'));
    }

    public function rolesPermissions()
    {
        $nameUser = auth()->user()->name;
        echo ("<h1>{$nameUser}</h1>");

        foreach( auth()->user()->roles as $role )
        {
            echo $role->name;

            $permissions = $role->permissions;
            foreach( $permissions as $permission)
            {
                echo " $permission->name";
            }
        }
    }
}
