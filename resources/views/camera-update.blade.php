@extends('layouts.app')

@section('content')
<div class="container">
    <div class="row">
        <div class="col-md-8 col-md-offset-2">
            <div class="panel panel-default">
                <div class="panel-heading">Dashboard</div>

                <div class="panel-body">
                    @if (session('status'))
                        <div class="alert alert-success">
                            {{ session('status') }}
                        </div>
                    @endif

                    <h1>{{$camera->title}}</h1>
                    <p>{{$camera->caminho}}</p>
                    <p><b>Author: {{$camera->user->name}}</b></p>


                    <img style="width: 100%;" class="img-responsive" src="http://201.28.98.51:80/axis-cgi/mjpg/video.cgi">

                </div>
            </div>
        </div>
    </div>
</div>
@endsection
