@extends('adminlte::page')

@section('title', 'LeadTI')

@section('content_header')
	<!doctype html>
	<html lang="en">
	<head>
	  <meta charset="utf-8">
	  <title>Player</title>
	  <base href="/">

	  <meta name="viewport" content="width=device-width, initial-scale=1">
	  <link rel="icon" type="image/x-icon" href="favicon.ico">
	  <script type="text/javascript" src="./painel/public/js/vxgplayer-1.8.31.min.js"></script>
	  <link href="./painel/public/js/vxgplayer-1.8.31.min.css" rel="stylesheet"/>
	</head>

@stop

@section('content')

	@forelse($cameras as $camera)
	<app-root></app-root>
	 	
		<div id="vxg_media_player2" class="vxgplayer" width="300" height="240" url="{{$camera->caminho}}" aspect-ratio latency="3000000" autostart controls audio="0" mute="1" debug></div>
		
		<script type="text/javascript" src="./painel/public/js/inline.bundle.js"></script>
		<script type="text/javascript" src="./painel/public/js/polyfills.bundle.js"></script>
		<script type="text/javascript" src="./painel/public/js/styles.bundle.js"></script>
		<script type="text/javascript" src="./painel/public/js/vendor.bundle.js"></script>
		<script type="text/javascript" src="./painel/public/js/main.bundle.js"></script>
	<h1>{{$camera->title}}</h1>
	<p>{{$camera->caminho}}</p>
	@empty
		<p> Nenhuma Camera cadastrada </p>

	@endforelse

@stop
